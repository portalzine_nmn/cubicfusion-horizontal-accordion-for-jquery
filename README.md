
cubicFUSION Horizontal Accordion for jQuery
=====================================
* * *
About
-----

The Horizontal Accordion provides a vast amount of options to adjust its settings to your liking and allows you to integrate it fast into your website. You have complete design freedom and can interact with the plugin through css, plugin methods and external function calls.

Usage
-----

More information : http://www.portalzine.de/projects/jquery-horizontal-accordion/

Documentation: http://www.portalzine.de/projects/jquery-horizontal-accordion/documentation/


License
-----------

/LICENSE.txt  
